from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class StatusConfig(AppConfig):
    name = "bizi_core.status"
    verbose_name = _("Status")

    # def ready(self):
    #     try:
    #         import bizi_core.products.signals  # noqa F401
    #     except ImportError:
    #         pass
