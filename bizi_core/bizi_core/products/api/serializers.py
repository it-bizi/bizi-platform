from django.apps import apps as django_apps
from rest_framework import serializers

ProductModel = django_apps.get_model("products.ProductModel", require_ready=False)


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModel
        fields = ['id', 'product_name', 'product_desc']
