from django.apps import apps as django_apps
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import (
    CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
)
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .serializers import ProductSerializer

ProductModel = django_apps.get_model("products.ProductModel", require_ready=False)


class ProductViewSet(
    RetrieveModelMixin, 
    ListModelMixin, 
    UpdateModelMixin, 
    CreateModelMixin, 
    DestroyModelMixin, 
    GenericViewSet
):
    serializer_class = ProductSerializer
    queryset = ProductModel.objects.all()
