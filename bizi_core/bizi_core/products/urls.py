from django.urls import path

from bizi_core.products.views import (
    product_detail_view
)


app_name = "products"
urlpatterns = [
    path("<int:id>/", view=product_detail_view, name="detail"),
]
