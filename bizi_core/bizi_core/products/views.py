from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView
from .models import ProductModel

class ProductDetailView(DetailView):
    model = ProductModel

product_detail_view = ProductDetailView.as_view()
