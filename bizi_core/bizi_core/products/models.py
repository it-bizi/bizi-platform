from django.db import models

class ProductModel(models.Model):
    product_name = models.CharField(max_length=50)
    product_desc = models.TextField(max_length=255)

    class Meta:
        db_table = "products"