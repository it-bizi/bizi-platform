from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ProductsConfig(AppConfig):
    name = "bizi_core.products"
    verbose_name = _("Products")

    # def ready(self):
    #     try:
    #         import bizi_core.products.signals  # noqa F401
    #     except ImportError:
    #         pass
