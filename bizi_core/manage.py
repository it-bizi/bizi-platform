#!/usr/bin/env python
import os
import sys
import yaml
import logging

from pathlib import Path
from yaml.loader import SafeLoader

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django  # noqa
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )

        raise

    # This allows easy placement of apps within the interior
    # bizi_core directory.
    current_path = Path(__file__).parent.resolve()
    sys.path.append(str(current_path / "bizi_core"))

    # Load variables into environment
    config_file_path = f"{current_path}/../bizi-configuration.yaml" 
    with open(config_file_path) as f:
        data = yaml.load(f, Loader=SafeLoader)
        for conf_key in data.keys():
            os.environ[conf_key] = data[conf_key]

    execute_from_command_line(sys.argv)
