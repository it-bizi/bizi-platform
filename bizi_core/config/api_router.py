from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from bizi_core.users.api.views import UserViewSet
from bizi_core.products.api.views import ProductViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("products", ProductViewSet)


app_name = "api"
urlpatterns = router.urls
