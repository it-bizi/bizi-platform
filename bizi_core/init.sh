#!/bin/sh

base_dir=`pwd`
cd bizi_core

echo "Installing python requirements"
pip install -r requirements/local.txt

echo "Migrating Database"
python manage.py migrate

echo "Starting DJango application"
python manage.py runserver 0.0.0.0:8000 < /dev/null > man.log &

loop_count=0
sleep_duration=5

while [ $loop_count -lt 10 ]
do
    status_code=`curl -o /dev/null -s -w "%{http_code}\n" http://localhost:8000/api/`
    if [ "$status_code" == "200" ]
    then
        echo "Application Server started successfully"
        break
    else
        echo "Application Server not yet started. Sleeping for $sleep_duration seconds"
        sleep $sleep_duration
        loop_count=$((loop_count+1))
    fi
done
