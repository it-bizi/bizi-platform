pipeline {
    agent any

    environment {
        BRANCH_NAME = "${env.GIT_BRANCH}"
        BIZI_CREDS = credentials('jenkinsbizi')
        PATH = "{$PATH}:/usr/local/bin"
    }

    options {
        // This limits build concurrency to 1 per branch
        disableConcurrentBuilds()
    }

    stages {
        stage('Init') {
            steps {
                bitbucketStatusNotify buildState: "INPROGRESS"
                script {
                    def branch_name = (env.BRANCH_NAME).replaceAll(" ", "")
                    def final_branch_name = ""
                    workspace_arr = env.WORKSPACE.split("/")
                    file_root_dir = workspace_arr[workspace_arr.size()-1]
                    backend_image_name = file_root_dir + "-backend-1"
                    if ( branch_name != "master") {
                        final_branch_name = "-" + branch_name
                    }
                    env.bizibackendimg = "bizibackendimg" + final_branch_name
                    env.bizifrontendimg = "bizifrontendimg" + final_branch_name
                    env.bizidatabaseimg = "bizidatabaseimg-" + branch_name
                    env.constructed_branch_name = branch_name
                    env.backend_image_name = backend_image_name
                    env.JUNIT_XML_FILE_NAME = backend_image_name + ".xml"
                    env.JUNIT_XML_FILE = "/var/tmp/" + JUNIT_XML_FILE_NAME
                    env.JENKINS_S3_FILE_PATH = "s3://" + JENKINS_BUCKET_NAME + JENKINS_S3_RESULTS_PATH + "/" + JUNIT_XML_FILE_NAME
                }
                sh "printenv"
            }
        }
        stage('Build') {
            steps {
                echo "Executing script for building Bizi Platform Backend image"
                sh './bizi_devops/scripts/CreateDockerImage.sh bizibackendimg ./bizi_core ${bizibackendimg}'

                echo "Executing script for building Bizi Platform Frontend image"
                sh './bizi_devops/scripts/CreateDockerImage.sh bizifrontendimg ./bizi_ui ${bizifrontendimg}'
            }
        }

        stage('Start Containers') {
            steps {
                sh "printenv"
                echo "Starting docker containers"
                sh 'docker-compose up -d'
            }
        }

        stage('Unit Test') {
            steps {
                echo "Starting Backend Unit tests"
                sh 'docker exec ${backend_image_name} /opt/bizi/products/bizi_core/bin/run_test.sh'
                sh 'aws s3 cp ${JENKINS_S3_FILE_PATH} ./backend-unit-test.xml'
            }
            post {
                always {
                    junit 'backend-unit-test.xml'

                    echo "Stopping docker containers"
                    sh 'docker-compose stop'
                }
            }
        }

        stage('Push Images') {
            when {
                branch 'master'
            }
            steps {
                echo "Executing script for Publishing Bizi Platform Backend image"
                sh './bizi_devops/scripts/PublishDockerImage.sh bizibackendimg ./bizi_core ${bizibackendimg}'

                echo "Executing script for Publishing Bizi Platform Frontend image"
                sh './bizi_devops/scripts/PublishDockerImage.sh bizifrontendimg ./bizi_ui ${bizifrontendimg}'

                echo "Remove unwanted local files"
                sh 'rm -rf ${PWD}/backend-unit-test.xml'

                echo "Committing version file"
                withCredentials([gitUsernamePassword(credentialsId: 'jenkinsbizi', gitToolName: 'git-tool')]) {
                    sh 'git add ${PWD}/bizi_core/info.txt ${PWD}/bizi_ui/info.txt'
                    sh 'git commit -m "Updated version number in info.txt"'
                    sh 'git push origin HEAD:${BRANCH_NAME}'
                }
             }
        }

        stage('Clean-up') {
            steps {
                echo "Removing docker containers"
                sh 'docker-compose down'
            }
        }

    }

    post {
        success {
            echo "Build executed with status as SUCCESS"
            bitbucketStatusNotify buildState: "SUCCESSFUL"
        }
        failure {
            echo "Build executed with status as FAILED"
            bitbucketStatusNotify buildState: "FAILED"
        }
    }
}