#!/bin/sh

base_dir=`pwd`
cd bizi_ui

echo "Starting Compilation"
npm i

echo "Starting React application"
npm start &

loop_count=0
sleep_duration=5

while [ $loop_count -lt 10 ]
do
    status_code=`curl -o /dev/null -s -w "%{http_code}\n" http://localhost:3000`
    if [ "$status_code" == "200" ]
    then
        echo "React UI Server started successfully"
        break
    else
        echo "React UI Server not yet started. Sleeping for $sleep_duration seconds"
        sleep $sleep_duration
        loop_count=$((loop_count+1))
    fi
done
