import React, { Component } from "react";
import { Table } from "reactstrap";
import NewProductModal from "./NewProductModal";

import ConfirmRemovalModal from "./ConfirmRemovalModal";

class ProductList extends Component {
  render() {
    const products = this.props.products;
    return (
      <Table dark>
        <thead>
          <tr>
            <th>id</th>
            <th>Product Name</th>
            <th>Product Desc</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {!products || products.length <= 0 ? (
            <tr>
              <td colSpan="4" align="center">
                <b>Ops, no one here yet</b>
              </td>
            </tr>
          ) : (
            products.map(product => (
              <tr key={product.pk}>
                <td>{product.id}</td>
                <td>{product.product_name}</td>
                <td>{product.product_desc}</td>
                <td align="center">
                  <NewProductModal
                    create={false}
                    product={product}
                    resetState={this.props.resetState}
                  />
                  &nbsp;&nbsp;
                  <ConfirmRemovalModal
                    pk={product.id}
                    resetState={this.props.resetState}
                  />
                </td>
              </tr>
            ))
          )}
        </tbody>
      </Table>
    );
  }
}

export default ProductList;