import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div className="text-center">
        <h1>BIZI PLATFORM</h1>
        <h5>
          <i>presents</i>
        </h5>
        <hr />
        <h3>App with React + Django</h3>
      </div>
    );
  }
}

export default Header;