import React from "react";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";

import axios from "axios";

import { PRODUCTS_API_URL } from "../constants";

class NewProductForm extends React.Component {
  state = {
    id: 0,
    product_name: "",
    product_desc: ""
  };

  componentDidMount() {
    if (this.props.product) {
      const { id, product_name, product_desc } = this.props.product;
      this.setState({ id, product_name, product_desc });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  createProduct = e => {
    e.preventDefault();
    axios.post(PRODUCTS_API_URL, this.state).then(() => {
      this.props.resetState();
      this.props.toggle();
    });
  };

  editProduct = e => {
    e.preventDefault();
    axios.put(PRODUCTS_API_URL + this.state.id + "/", this.state).then(() => {
      this.props.resetState();
      this.props.toggle();
    });
  };

  defaultIfEmpty = value => {
    return value === "" ? "" : value;
  };

  render() {
    return (
      <Form onSubmit={this.props.product ? this.editProduct : this.createProduct}>
        <FormGroup>
          <Label for="name">Product Name:</Label>
          <Input
            type="text"
            name="product_name"
            onChange={this.onChange}
            value={this.defaultIfEmpty(this.state.product_name)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="email">Product Desc:</Label>
          <Input
            type="text"
            name="product_desc"
            onChange={this.onChange}
            value={this.defaultIfEmpty(this.state.product_desc)}
          />
        </FormGroup>
        <Button>Send</Button>
      </Form>
    );
  }
}

export default NewProductForm;