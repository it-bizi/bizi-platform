#!/bin/sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                                 PARAMETERS                                ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

imageName=$1
imageTag=$2
dockerImage=${imageName}:${imageTag}

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                      VERIFY AND REMOVE DOCKER IMAGE                       ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#:: Remove image (if exists)
dockerImageCount=`docker images | grep ${imageName} | grep ${imageTag} | wc -l`
if [[ ${dockerImageCount} > 0 ]]; then
	docker rmi -f ${dockerImage}
	echo "Removed docker image - ${dockerImage}"
fi
