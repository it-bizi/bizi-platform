#!/bin/sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                               CONFIGURATION                               ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# docker_file_name=Dockerfile
current_working_dir=`pwd`
increment_version=0

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                                 PARAMETERS                                ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

local_image=$1
info_file_path=$2
branch_image_name=$3

# Validate if the image name is passed
if [ -z "${local_image}" ]; then
    echo "Image name parameter is missing"
    exit
fi

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             DEFAULT INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

CUR_SCRIPT_BASE_DIR=`dirname $0`
SCRIPTS_BASE_DIR=`readlink -f "${CUR_SCRIPT_BASE_DIR}"`
find_str="VERSION_NUMBER"
devops_path=`readlink -f "${SCRIPTS_BASE_DIR}/../"`
image_file_path="${devops_path}/${local_image}"
info_file_name="info.txt"

BIZI_PLATFORM_BASE_DIR=`readlink -f ${SCRIPTS_BASE_DIR}/../../`

# Validate if the image file path exists
if [ ! -d "${image_file_path}" ]; then
    echo "Invalid Image name - ${local_image}"
    exit
fi

# Validate if the info file path is specified
if [ -z ${info_file_path} ]; then
    info_file_path=${image_file_path}
else
    info_file_path=`readlink -f ${info_file_path}`
fi
info_file=${info_file_path}/${info_file_name}
echo "Info file - ${info_file}"

# Validate if the info file exists
if [ ! -f ${info_file} ]; then
    echo "Info file does not exists for - ${local_image}"
    exit
fi

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                        PERFORM BASE INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "Scripts Base Directory: ${SCRIPTS_BASE_DIR}"
cd ${SCRIPTS_BASE_DIR}

source ./BaseInit.sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             PUSH DOCKER IMAGE                             ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Get the version number
build_version_number=`grep "${find_str}"  ${info_file} | cut -d"=" -f2`

# Push docker images
sh ./PushDockerImage.sh ${local_image} ${build_version_number} ${branch_image_name}

cd ${current_working_dir}
