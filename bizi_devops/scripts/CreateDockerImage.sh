#!/bin/sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                               CONFIGURATION                               ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

docker_file_name=Dockerfile
current_working_dir=`pwd`

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                                 PARAMETERS                                ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

local_image=$1
info_file_path=$2
branch_name=$3

# Validate if the image name is passed
if [ -z "${local_image}" ]; then
    echo "Image name parameter is missing"
    exit
fi

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             DEFAULT INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

CUR_SCRIPT_BASE_DIR=`dirname $0`
SCRIPTS_BASE_DIR=`readlink -f "${CUR_SCRIPT_BASE_DIR}"`
find_str="VERSION_NUMBER"
devops_path=`readlink -f "${SCRIPTS_BASE_DIR}/../"`
image_file_path="${devops_path}/${local_image}"
info_file_name="info.txt"

BIZI_PLATFORM_BASE_DIR=`readlink -f ${SCRIPTS_BASE_DIR}/../../`
additional_args="--build-arg BIZI_PLATFORM_BASE_DIR=${BIZI_PLATFORM_BASE_DIR} --no-cache=true"
export BIZI_PLATFORM_BASE_DIR=${BIZI_PLATFORM_BASE_DIR}
export ADDITIONAL_ARGS=${additional_args}

# Validate if the image file path exists
if [ ! -d "${image_file_path}" ]; then
    echo "Invalid Image name - ${local_image}"
    exit
fi

# Validate if the info file path is specified
if [ -z ${info_file_path} ]; then
    info_file_path=${image_file_path}
else
    info_file_path=`readlink -f ${info_file_path}`
fi
info_file=${info_file_path}/${info_file_name}
echo "Info file - ${info_file}"

# Validate if the info file exists
if [ ! -f ${info_file} ]; then
    echo "Info file does not exists for - ${local_image}"
    exit
fi

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                        PERFORM BASE INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "Scripts Base Directory: ${SCRIPTS_BASE_DIR}"
cd ${SCRIPTS_BASE_DIR}

source ./BaseInit.sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                       DOCKER IMAGE CREATION PROCESS                       ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Update version number if push image is set to True
if [ "${BRANCH_NAME}" == "master" ]; then
    # Get the version number
    version_number=`grep "${find_str}"  ${info_file} | cut -d"=" -f2`
    
    # construct the new version number
    buildVersion=`echo ${version_number} | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}'`
    
    # Update the version number
    sed -i "s/${find_str}=${version_number}/${find_str}=${buildVersion}/g" ${info_file}
    echo "Incremented version number to ${buildVersion}"
else
    echo "Version increment will be skipped since the branch name is not master - (${BRANCH_NAME})"
fi

# Create docker image
sh ./ProcessDockerCreateImage.sh ${local_image} ${docker_file_name} ${branch_name}

cd ${current_working_dir}
