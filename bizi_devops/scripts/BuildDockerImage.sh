#!/bin/sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                                 PARAMETERS                                ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

local_dir_name=$1
docker_file_name=$2
final_image_name=$3
image_scripts_path=./../${local_dir_name}
image_scripts_dir=`realpath ${image_scripts_path}`
current_working_dir=`pwd`

echo "Starting Docker build process for ${final_image_name}"

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             CREATE DOCKER IMAGE                           ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Change directory to the Docker batch file location
echo "Changing directory to ${image_scripts_dir}"
cd ${image_scripts_dir}

# Build base docker image
docker build -f ${docker_file_name} -t ${final_image_name} ${ADDITIONAL_ARGS} ${BIZI_PLATFORM_BASE_DIR}
echo "Created docker image with name ${final_image_name}"

# Set current directory to base script location
echo "Changing directory to ${current_working_dir}"
cd ${current_working_dir}
