#!/bin/sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             PERFORM DOCKER LOGIN                          ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "Retrieving AWS Login command"

# Capture login command
login_cmd=`aws ecr get-login --no-include-email --region eu-west-1`

# Perform Login
echo "Performing AWS Login"
${login_cmd}

echo "AWS Login Process Completed"