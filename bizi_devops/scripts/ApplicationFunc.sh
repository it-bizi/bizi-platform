#!/bin/sh

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::                                  STOP AND REMOVE - CONTAINER & IMAGE                             ::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function stopAndRemove()
{
	container_name=$1
	repository_image_name=$2
    sleep_duration=5

	echo "Validating Server Container Status (${container_name})"
	container_exists_status=`docker ps -a | grep "${container_name}" | wc -l`
		
	# Validating Running Status
	if [ ${container_exists_status} -gt 0 ]; then
		sleep ${sleep_duration}

		# Stop Container
		docker stop ${container_name}
		echo "Stopped '${container_name}' Docker Container"

		sleep ${sleep_duration}
	
		# Remove Container
		docker rm -f ${container_name}
		echo "Removed '${container_name}' Docker Container"

		sleep ${sleep_duration}
	else
		echo "Running Container (${container_name}} does not exists"
	fi
	
	# Remove Image
	if [ ! -z "${repository_image_name}" ]; then
		docker rmi -f ${repository_image_name}
		echo "Removed '${repository_image_name}' Docker Image"
	fi
}

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::                                            STOP CONTAINER                                        ::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function stopContainer()
{
	container_name=$1
	repository_image_name=$2
    sleep_duration=5
	
	while true
	do
		echo "Validating Server Container Status (${container_name})"
		container_running_status=`docker ps -a | grep "${container_name}" | grep -i "Exited" | wc -l`
		
		# Validating Container Running Status
		if [ ${container_running_status} -gt 0 ]; then
			echo "Server Container (${container_name}) has Exited"
			break
		else
			echo "Server Container (${container_name}) is running"
			sleep ${sleep_duration}
		fi
	done

	# Stop And Remove
	stopAndRemove "${container_name}" "${repository_image_name}"
}
