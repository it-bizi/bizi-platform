#!/bin/sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                                 PARAMETERS                                ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

local_image=$1
build_version=$2
branch_image_name=$3


#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             DEFAULT INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#:: Perform Base Initialization
source ./BaseInit.sh

repository_image=${REPOSITORY_BASE_LOCATION}/${local_image}
repository_image_version_tag=${repository_image}:${build_version}
repository_image_latest_tag=${repository_image}:latest
local_image_latest_tag=${branch_image_name}:latest

echo "Starting Docker Push Process for Image: '${branch_image_name}' Repository: '${repository_image}' Build Version: '${build_version}'"

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                               PERFORM AWS LOGIN                           ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

source ./PerformAWSLogin.sh

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::               REMOVE EXISTING LATEST TAG FROM REPOSITORY                  ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Check and remove image with latest tag from repository (if exists)
repository_imageCount=`aws ecr list-images --repository-name ${local_image} | grep "imageTag" | grep "latest" | wc -l`
if [[ ${repository_imageCount} > 0 ]]; then
	echo "Deleting latest image from repository - '${repository_image}'"
	aws ecr batch-delete-image --repository-name ${local_image} --image-ids imageTag=latest || true
fi

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                              CREATE DOCKER TAGS                           ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Tag created image as latest
echo "Creating tag '${repository_image_latest_tag}' from '${local_image_latest_tag}'"
docker tag ${local_image_latest_tag} ${repository_image_latest_tag}

# Tag created image as build version
echo "Creating tag '${repository_image_version_tag}' from '${local_image_latest_tag}'"
docker tag ${local_image_latest_tag} ${repository_image_version_tag}

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                              PUSH DOCKER IMAGE                            ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Push docker latest image to repository
echo "Pushing image ${repository_image_latest_tag}"
docker push ${repository_image_latest_tag}

# Push docker version image to repository
echo "Pushing image ${repository_image_version_tag}"
docker push ${repository_image_version_tag}

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                            REMOVE DOCKER IMAGE                            ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

sh ./RemoveDockerImage.sh ${repository_image} ${build_version}
sh ./RemoveDockerImage.sh ${repository_image} latest
sh ./RemoveDockerImage.sh ${branch_image_name} latest

echo "------------------------------------------------------------------------------------------------------"
echo "Completed Docker Push Process for ${branch_image_name}"
