#!/bin/bash

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             DEFAULT INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

PARAM_1=$1
BIN_DIR=`dirname $0`
APP_BASE_DIR_NAME=bizi_ui
APP_BASE_DIR=`readlink -f ${BIN_DIR}/../`

if [ -z "${PARAM_1}" ]; then
    PARAM_1="START"
fi

if [ "${PARAM_1}" == "START" ]; then

    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #::::                            PERFORM COMPILATION                            ::::
    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    export NVM_DIR="$HOME/.nvm" && \
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  && \

    cd ${APP_BASE_DIR}

    echo "Starting Compilation"
    npm i

    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #::::                             START APPLICATION                             ::::
    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    echo "Starting React application"
    npm start

else
    echo "Welcome to bizi_ui..."
    ${PARAM_1}
fi