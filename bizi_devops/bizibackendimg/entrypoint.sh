#!/bin/bash

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             DEFAULT INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

PARAM_1=$1
BIN_DIR=`dirname $0`
APP_BASE_DIR_NAME=bizi_core
APP_BASE_DIR=`readlink -f ${BIN_DIR}/../`

if [ -z "${PARAM_1}" ]; then
    PARAM_1="START"
fi

if [ "${PARAM_1}" == "START" ]; then

    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #::::                              MIGRAGE DATABASE                             ::::
    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    cd ${APP_BASE_DIR}
    echo "Changing directory to ${APP_BASE_DIR}"

    echo "Migrating Database"
    python manage.py migrate

    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #::::                             START APPLICATION                             ::::
    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    echo "Starting DJango application"
    python manage.py runserver 0.0.0.0:8000

else
    echo "Welcome to bizi_core..."
    ${PARAM_1}
fi