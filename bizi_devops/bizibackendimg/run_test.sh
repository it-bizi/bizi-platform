#!/bin/bash

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             DEFAULT INITIALIZATION                        ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "XML file - ${JUNIT_XML_FILE}"
current_dir=`pwd`

BIN_DIR=`dirname $0`
APP_BASE_DIR_NAME=bizi_core
APP_BASE_DIR=`readlink -f ${BIN_DIR}/../`

echo "Changing directory to ${APP_BASE_DIR}"
cd ${APP_BASE_DIR}

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                             EXECUTE UNIT TEST                             ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "Executing Python Unit Test"
py.test -s --junit-xml ${JUNIT_XML_FILE}

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                          COPY FILE TO S3 BUCKET                           ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "Copying ${JUNIT_XML_FILE} to ${JENKINS_S3_FILE_PATH}"
aws s3 cp ${JUNIT_XML_FILE} ${JENKINS_S3_FILE_PATH}

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::                         CHANGING TO CURRENT DIR                           ::::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

cd ${current_dir}